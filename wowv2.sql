-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Värd: 127.0.0.1
-- Tid vid skapande: 04 okt 2019 kl 12:45
-- Serverversion: 10.4.6-MariaDB
-- PHP-version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databas: `wowv2`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `class_tabel`
--

CREATE TABLE `class_tabel` (
  `class_id` int(11) NOT NULL,
  `class_abilities1` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `class_tabel`
--

INSERT INTO `class_tabel` (`class_id`, `class_abilities1`, `class_name`) VALUES
(1, 'fireball', 'mage'),
(2, 'charge', 'warrior');

-- --------------------------------------------------------

--
-- Tabellstruktur `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `user`
--

INSERT INTO `user` (`user_id`, `login_name`, `password`) VALUES
(1, 'Svante99', '123'),
(2, 'betty88', '2019'),
(3, 'oskar', 'qwerty'),
(4, 'noob', 'letmein'),
(5, 'sigges', '1231231313'),
(6, 'svante2', '1231231313'),
(7, 'svante3', '1231231313'),
(8, 'svante4', '1231231313');

-- --------------------------------------------------------

--
-- Tabellstruktur `user_characters`
--

CREATE TABLE `user_characters` (
  `characters_id` int(11) NOT NULL,
  `chracter_level` int(11) DEFAULT NULL,
  `character_names` varchar(255) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `user_characters`
--

INSERT INTO `user_characters` (`characters_id`, `chracter_level`, `character_names`, `class_id`, `user_id`) VALUES
(1, 99, 'Verog', 2, 1),
(2, 99, 'VerogTheDestroyer', 2, 2),
(3, 99, 'Serpentis', 1, 2),
(4, 99, 'slayer99', 1, 4),
(5, 99, 'slayer81', 2, 1),
(6, 99, 'slayer81999', 1, 7),
(7, 99, 'slayer6666', 1, 8);

--
-- Index för dumpade tabeller
--

--
-- Index för tabell `class_tabel`
--
ALTER TABLE `class_tabel`
  ADD PRIMARY KEY (`class_id`);

--
-- Index för tabell `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Index för tabell `user_characters`
--
ALTER TABLE `user_characters`
  ADD PRIMARY KEY (`characters_id`),
  ADD KEY `FKbye5gop11kt8qkkexplaepdtv` (`class_id`),
  ADD KEY `FKlhg6q7514ieity8rfwuqxydv4` (`user_id`);

--
-- AUTO_INCREMENT för dumpade tabeller
--

--
-- AUTO_INCREMENT för tabell `class_tabel`
--
ALTER TABLE `class_tabel`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT för tabell `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT för tabell `user_characters`
--
ALTER TABLE `user_characters`
  MODIFY `characters_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

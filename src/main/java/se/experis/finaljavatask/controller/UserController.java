package se.experis.finaljavatask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.experis.finaljavatask.model.User;
import se.experis.finaljavatask.repository.UserRepository;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @GetMapping("/add")
    public String addUser() {
        User aUser = new User();
        userRepository.save(aUser);
        return "200";
    }

    @PostMapping("/addpost")
    @ResponseBody
    public User addUser(@RequestBody User user) {
        System.out.println(user);
        return userRepository.save(user);
    }


    @GetMapping("/user")
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @GetMapping("/user/{userId}")
    public User getUser(@PathVariable int userId){
        return userRepository.findUserByuserId(userId);
    }




}

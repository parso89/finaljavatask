package se.experis.finaljavatask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.experis.finaljavatask.model.UserCharacters;
import se.experis.finaljavatask.repository.UserCharactersRepository;
import java.util.List;


@RestController
public class UserCharactersController {

    @Autowired
    private final UserCharactersRepository userCharactersRepository;

    public UserCharactersController(UserCharactersRepository userCharactersRepository) {
        this.userCharactersRepository = userCharactersRepository;
    }

    @GetMapping("/addcharacter")
    public String addCharacter() {
        UserCharacters aUserCharacter = new UserCharacters();
        userCharactersRepository.save(aUserCharacter);
        return "200";
    }

    @PostMapping("/addpostchar")
    @ResponseBody
    public UserCharacters addchar(@RequestBody UserCharacters userCharacters) {
        System.out.println(userCharacters);
        return userCharactersRepository.save(userCharacters);
    }

    @GetMapping("/user/{charactersId}")
    public UserCharacters getcharacter(@PathVariable int charId){
        return userCharactersRepository.findUserCharactersBycharactersId(charId);
    }

    @GetMapping("/usercharacters")
    public List<UserCharacters> getAll() {
        return userCharactersRepository.findAll();
    }

}

package se.experis.finaljavatask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import se.experis.finaljavatask.model.ClassTabel;
import se.experis.finaljavatask.repository.ClassTabelRepository;

import java.util.List;


@RestController
public class ClassTabelController {

    @Autowired
    private final ClassTabelRepository classTabelRepository;

    public ClassTabelController(ClassTabelRepository classTabelRepository) {
        this.classTabelRepository = classTabelRepository;
    }

    @GetMapping("/addclass")
    public String addClass() {
        ClassTabel aClass = new ClassTabel();
        classTabelRepository.save(aClass);
        return "200";
    }

    @GetMapping("/classes")
    public List<ClassTabel> getAll() {
        return classTabelRepository.findAll();
    }

    @GetMapping("/clsses/{className}")
    public ClassTabel getclass (@PathVariable String className){
        return classTabelRepository.findClassTabelByclassName(className);
    }


}

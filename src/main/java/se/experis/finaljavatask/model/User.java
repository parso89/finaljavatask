package se.experis.finaljavatask.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userId")
    public int userId;

    @Column(name = "loginName")
    public String loginName;

    @Column(name = "password")
    public String password;

    public User(String loginName, String password) {
        userId = 0;
        this.loginName = loginName;
        this.password = password;
    }

    public User() {
    loginName = "Svante";
    password = "2019";
    }

    public List<UserCharacters> getUserCharacters() {
        return userCharacters;
    }

    /* @OneToMany(fetch = FetchType.LAZY ,mappedBy = "charId")
    List<UserCharacters> userCharacters; */

   @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserCharacters> userCharacters;

}


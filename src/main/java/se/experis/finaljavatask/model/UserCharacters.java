package se.experis.finaljavatask.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class UserCharacters {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "charactersId")
    public int charactersId;

    @Column(name = "characterNames")
    public String characterNames;

    @Column(name = "chracterLevel")
    public int characterLevel;

    @Column(name = "userId")
    public int userId;

    @Column(name = "classId")
    public int classId;

    public UserCharacters( String characterNames, int characterLevel, int userId, int classId) {
        charactersId = 0;
        this.characterNames = characterNames;
        this.characterLevel = characterLevel;
        this.userId = userId;
        this.classId = classId;
    }

    public UserCharacters() {
        characterNames = "VerogTheDestroyer";
        characterLevel = 99;
    }

   /* @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charId")
    public User charId;*/

   @ManyToOne(optional = false)
   @JoinColumn(name = "userId", referencedColumnName = "userId", insertable = false, updatable = false)
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "classId", referencedColumnName = "classId", insertable = false, updatable = false)
    private ClassTabel charclass;

}

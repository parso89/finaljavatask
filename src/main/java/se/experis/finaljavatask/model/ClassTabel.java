package se.experis.finaljavatask.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class ClassTabel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "classId")
    public int classId;

    @Column(name = "className")
    public String className;

    @Column(name = "classAbilities1")
    public String classAbilities1;

    public ClassTabel(String className, String classAbilities1) {
        classId = 0;
        this.className = className;
        this.classAbilities1 = classAbilities1;
    }

    public ClassTabel() {
        this.className = "mage";
        this.classAbilities1 = "fireball";
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "charclass")
    private List<UserCharacters> classTabel;

}

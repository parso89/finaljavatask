package se.experis.finaljavatask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinaljavataskApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinaljavataskApplication.class, args);
	}

}

package se.experis.finaljavatask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.finaljavatask.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserByuserId(int userId);
}

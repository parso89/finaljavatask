package se.experis.finaljavatask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.finaljavatask.model.UserCharacters;

public interface UserCharactersRepository extends JpaRepository<UserCharacters, Integer> {
    UserCharacters findUserCharactersBycharactersId(int charactersId);
}

package se.experis.finaljavatask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.finaljavatask.model.ClassTabel;


public interface ClassTabelRepository extends JpaRepository<ClassTabel, Integer> {
    ClassTabel findUserCharactersByclassId(int classId);
    ClassTabel findClassTabelByclassName(String className); // 1:className är variablen 2:är collumnen
}
